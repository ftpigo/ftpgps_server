package com.ftp.map.testmapgps.dao;

import java.util.ArrayList;

import com.ftp.map.testmapgps.dto.mapDTO;

public interface mapDAO {

	public ArrayList<mapDTO> listDao();
	public void writeDao(String latitude,String longitude);
	/*public mapDTO viewDao(String strID);
	public void deleteDao(String bId);*/
	
}
