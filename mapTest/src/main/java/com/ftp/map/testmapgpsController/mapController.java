package com.ftp.map.testmapgpsController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ftp.map.testmapgps.dao.mapDAO;
import com.ftp.map.testmapgps.dto.mapDTO;

public class mapController {

	private SqlSession sqlSession;
	
	/*안드로이드 통신관련*/
	@RequestMapping("/mapgps")
	@ResponseBody
	public Map<String, String> androidTestWithRequestAndResponse(HttpServletRequest request){

	        ArrayList<mapDTO> mapDto = new ArrayList<mapDTO>();
	        Map<String, String> result = new HashMap<String, String>();
	        String nowTime = getCurrentTime("YYYY,M,d");
	        String latitude = "", longitude = "";
	      
	        mapDAO dao = sqlSession.getMapper(mapDAO.class);
	       
	        dao.writeDao(nowTime,request.getParameter("content"));
	        
	        mapDto = dao.listDao();
	        
	        for(int i=0;i<mapDto.size();i++) {
	        	latitude += mapDto.get(i).getLatitude()+"\t";
	        	longitude += mapDto.get(i).getLongitude()+"\t";
	        }

	        result.put("latitude", latitude);
	        result.put("longitude", longitude);

	        return result;
	}

	private String getCurrentTime(String timeFormat) {
	         // TODO Auto-generated method stub
	         return new SimpleDateFormat(timeFormat).format(System.currentTimeMillis());
	}
	
}
